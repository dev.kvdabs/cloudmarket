import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { AlertController, ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {

  process = 1;


  subtotal = 0;
  deliveryFee = 0;
  total = 0;

  deliveryOption;
  deliveryOptionDisplay = '';

  deliveryInfo: {
    name: string,
    mobileNo: string,
    address: string,
    waze: string
  };

  pickupInfo = [
    {
      key: 'Address',
      value: '026-BLK-12 Veterans Drive, Brgy. Busay Cebu City'
    }, 
    // {
    //   key: 'Contact',
    //   value: 'Jeralda (09271234567)'
    // },
    // {
    //   key: 'Waze',
    //   value: 'Aeon\'s Dessert House'
    // }
  ];

  gcash= {
    account: '09272645030'
  }

  paymentMethod: number;
  paymentMethodDisplay = '';

  deliveryInfoData: FormGroup;

  disableNextButton: boolean = true;

  constructor(private cartService: CartService, private toast: ToastController, private fb: FormBuilder) { 
    this.subtotal = this.cartService.cart_total;   

    this.deliveryInfoData = this.fb.group({
      name: ['', Validators.required],
      mobileNo: ['09272645030', Validators.required],
      address: ['026-BLK12 Veterans Drive, Busay Cebu City', Validators.required],
      waze: ['Aeon\'s Dessert House', Validators.required],
    });

    this.deliveryInfo = this.deliveryInfoData.value;

       
  }

  ngOnInit() {
  }

  deliverOptionChanged(event) {
    if (event && event.detail && event.detail.value == 2) {
      //this.deliveryFee = 30;
    }  
    else this.deliveryFee = 0;

    this.disableNextButton = false;

    this.computeTotal();
  }

  paymentMethodChanged(event) {
    if (event && event.detail) {
      this.disableNextButton = false;
    }    
  }

  computeTotal() {
    this.total = this.subtotal + this.deliveryFee;
  }

  back() {
    this.process--;
  }

  submit() {
    
  }

  next() {
    
    if (this.process == 1) {

      if (this.deliveryOption == 2) {
        if (!this.deliveryInfoData.valid) {
          this.showToastMessage("Please fill up delivery info", 'danger');
          return;
        }
      }

      this.deliveryInfo = this.deliveryInfoData.value;
      this.deliveryOptionDisplay = this.deliveryOption == 1 ? 'Pickup' : this.deliveryOption == 2 ? 'Deliver' : '';       

    } else if (this.process == 2) {
      this.paymentMethodDisplay = this.paymentMethod == 1? 'CASH' : this.paymentMethod == 2 ? 'G-CASH' : this.paymentMethod == 3 ? 'BANK TRANSFER' : '';
    }

    this.process++;
    this.disableNextButton = true;
  }

  async showToastMessage(message, color) {
    const toast = await this.toast.create({
      message: message,
      mode: 'ios',
      duration: 5000,
      color: color
    });

    toast.present();
  }
}
