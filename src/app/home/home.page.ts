import { Component } from '@angular/core';
import { NavController, ToastController, PickerController,  } from '@ionic/angular';
import { formatCurrency } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  CART_KEY = '_cloudmarket-cart';

  selectedCategory = 'Featured';
  categories = [
    'Featured', 'Pork', 'Beef', 'Chicken', 'Canned Goods', 'Bread', 'Juice', 'Biscuit', 'Noodles', 'Beer'
  ]

  items =[
    {
      id: 1,
      name: 'Holiday Corned Beef',
      price: 30.00,
      image: 'assets/imgs/1.jpg'
    },
    {
      id: 2,
      name: 'LM Pancit Canton Extra Hot Chili',
      price: 10.00,
      image: 'assets/imgs/2.jpg'
    }
    ,
    {
      id: 3,
      name: '555 Sardines',
      price: 18.00,
      image: 'assets/imgs/3.png'
    },
    {
      id: 4,
      name: 'Magic Sarap 30ML',
      price: 5.00,
      image: 'https://firebasestorage.googleapis.com/v0/b/shopnimo.appspot.com/o/items%2Fimages%2Fthumb145_10148?alt=media&token=fbff423f-914d-4c4c-9f66-74dd6ebaf9eb'
    },
    {
      id: 5,
      name: 'Silver Swan Suka 200ML',
      price: 12.00,
      image: 'assets/imgs/5.jpg'
    }
    ,
    {
      id: 6,
      name: 'Silver Swan Soy Sauce 200ML',
      price: 12.00,
      image: 'assets/imgs/6.jpg'
    }
  ]

  carts = [];
  cartCount = 0;

  constructor(private nav: NavController, private toast: ToastController, private picker: PickerController) {
        
  }

  imageLoaded(item) {
    console.log(item)
  }

  ionViewWillEnter() {
    const carts = localStorage.getItem(this.CART_KEY);
    if (carts) this.carts = JSON.parse(carts);    
    
    this.countCart();

    this.items.forEach(x => {
      x['quantity'] = 0
    })

    this.carts.forEach(x => {
      let item = this.items.find(i => i.id == x.id);
      if (item) {
        item['quantity'] = x.quantity;
      }
    });
  }

  account() {
    this.nav.navigateRoot('/seller')
  }

  select(item) {        
    item.quantity = 1;

    this.addToCart(item);
    this.countCart();
  }

  increment(item) {
    item.quantity++;    
    this.updateCart(item);
  }

  decrement(item) {
    item.quantity--;

    if (item.quantity <= 0) {
      item.selected = false;

      this.carts = this.carts.filter(x => x.id !== item.id);
      this.countCart();
    }

    this.updateCart(item);
  }

  countCart() {
    this.cartCount = this.carts.length;
  }

  addToCart(item) {
    item['total'] = item.price * item.quantity;
    this.carts.push(item);
    this.saveToStorage();
  }

  updateCart(item) {
    let cartItem = this.carts.find(x => x.id == item.id);
    if (cartItem) {
      cartItem['price'] = item.price;
      cartItem['quantity'] = item.quantity;

      cartItem['total'] = item.price * item.quantity;      
    } 

    this.saveToStorage();
  }

  saveToStorage() {
    localStorage.setItem(this.CART_KEY, JSON.stringify(this.carts));

    //this.showToast();
  }

  gotoCart() {    
    this.nav.navigateForward("/cart")
  }

  async showToast() {
    const total = this.carts.reduce( (tot, record) => {
      return tot + record['total'];
    }, 0);

    const toast = await this.toast.create({
      color: 'primary',
      message: 'Cart Total: ' + formatCurrency(total, 'en', ''),
      mode: 'ios',
      duration: 1000,
      position: 'top'
    });

    toast.present();
  }

  async selectCategory(category) {
    this.selectedCategory = category;
  }
}
