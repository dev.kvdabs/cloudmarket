import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  CART_KEY = '_cloudmarket-cart';

  constructor(private loading: LoadingController) { }

  get cart_total() {
    const cart = localStorage.getItem(this.CART_KEY) ? JSON.parse(localStorage.getItem(this.CART_KEY)) : [];

    return cart.reduce( (tot, record) => {
      return tot + record['total'];
    }, 0);
  }

  async submit() {
    const loading = await this.loading.create({
      message: 'Processing...',
      mode: 'ios'
    });

    loading.present();

    setTimeout(() => {
      this.clear();
      loading.dismiss();
    }, 3000);
  }

  clear() {
    localStorage.removeItem(this.CART_KEY)
  }
}
