import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  ITEM_KEY = 'items';

  items = [];

  constructor(private storage: Storage) { 
    this.getItems();
  }

  async getItems() {
    this.items = (await this.storage.get(this.ITEM_KEY)) || [];

    return this.items;
  }

  add (item) {
    this.items.push(item);
    this.storage.set(this.ITEM_KEY, this.items);
  }

  update(item) {
    this.items = this.items.filter(x => x.id !== item.id);
    this.items.push(item);

    this.storage.set(this.ITEM_KEY, this.items);
  }
}
