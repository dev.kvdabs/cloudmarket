import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SellerService {

  private _seller: any;

  constructor() { 
    let _seller = localStorage.getItem('seller');
    if (_seller) {
      this._seller = JSON.parse(_seller);
    }
  }

  get seller() {
    return this._seller;
  }

  get selectedDeliveryMethods(): number[] {
    if (this._seller && this._seller.deliveryMethods) return this._seller.deliveryMethods.map(x => x.id)

    return []
  }

  get deliveryMethodsDisplay() {
    if (this._seller && this._seller.deliveryMethods) return this._seller.deliveryMethods.map(x => x.name).join(', ')

    return '';
  }

  get paymentMethodsDisplay() {
    if (this._seller && this._seller.paymentMethods) return this._seller.paymentMethods.map(x => x.name).join(', ')

    return '';
  }

  get selectedPaymentMethods(): number[] {
    if (this._seller && this._seller.paymentMethods) return this._seller.paymentMethods.map(x => x.id)

    return []
  }

  update(props) {    

    for (var prop in props) {
      this._seller[prop] = props[prop];
    }

    localStorage.setItem('seller', JSON.stringify(this._seller))
  }
}
