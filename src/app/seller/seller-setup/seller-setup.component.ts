import { Component, OnInit } from '@angular/core';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { SellerService } from 'src/app/services/seller.service';
import { DeliveryComponent } from '../delivery/delivery.component';
import { PaymentMethodComponent } from '../payment-method/payment-method.component';

@Component({
  selector: 'app-seller-setup',
  templateUrl: './seller-setup.component.html',
  styleUrls: ['./seller-setup.component.scss'],
})
export class SellerSetupComponent implements OnInit {
  seller: any;
  deliveryMethodsDisplay = '';
  paymentMethodsDisplay: string;

  constructor(private modal: ModalController, private actionSheet: ActionSheetController, private sellerService: SellerService) { 
    this.seller = this.sellerService.seller;
    
    this.setDeliveryMethods();
    this.setPaymentMethods();

  }

  ngOnInit() {
  }

  ionViewWillLeave() {
    this.sellerService.update(this.seller);
  }

  setDeliveryMethods() {
    this.deliveryMethodsDisplay = this.sellerService.deliveryMethodsDisplay;
  }

  setPaymentMethods() {
    this.paymentMethodsDisplay = this.sellerService.paymentMethodsDisplay;
  }

  async delivery() {
    const modal = await this.modal.create({
      component: DeliveryComponent
    });

    modal.onDidDismiss().then(res => {
      if (res && res.data) this.setDeliveryMethods();
    })

    modal.present();
  }

  async paymentMethod() {
    const modal = await this.modal.create({
      component: PaymentMethodComponent
    });

    modal.onDidDismiss().then(res => {
      if (res && res.data) this.setPaymentMethods();
    })

    modal.present();
  }

  async openModal(component) {
    const modal = await this.modal.create({
      component: component
    });

    return modal.present();
  }
}
