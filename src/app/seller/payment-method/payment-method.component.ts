import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, LoadingController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators, MinLengthValidator } from '@angular/forms';
import { SellerService } from 'src/app/services/seller.service';

@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.scss'],
})
export class PaymentMethodComponent implements OnInit {

  options = [
    {
      id: 1,
      name: 'COD',
      selected: false
    },
    {
      id: 2,
      name: 'G-Cash',
      details: {},
      selected: false
    },
    {
      id: 3,
      name: 'Bank Transfer',
      details: {},
      selected: false
    }
  ]
  
  frmGcash: FormGroup;
  frmBankTransfer: FormGroup;

  constructor(private modal: ModalController, private fb: FormBuilder, private toast: ToastController, private loading: LoadingController, private sellerService: SellerService) {

    
  }

  ngOnInit() {

    this.sellerService.selectedPaymentMethods.forEach(id => {
        let option = this.options.find(x => x.id == id)
        option.selected = true;
    });
    
    this.frmBankTransfer = this.fb.group({
      bank: ['', Validators.required],
      accountNo: ['', Validators.required]
    });

    this.frmGcash = this.fb.group({
      account: ['', [Validators.required, Validators.minLength(11)]]
    });

    if (this.options.find(x => x.id == 2 && x.selected)) {
      const gcash = this.sellerService.seller.paymentMethods.find(x => x.id == 2);
      this.initForm(this.frmGcash, gcash.details)
    }

    if (this.options.find(x => x.id == 3 && x.selected)) {
      const bankTransfer = this.sellerService.seller.paymentMethods.find(x => x.id == 3);
      this.initForm(this.frmBankTransfer, bankTransfer.details)
    }
  }

  initForm(frm, data) {
    for (var key in data) {
      if (frm.controls[key]) frm.controls[key].setValue(data[key])
    }
  }

  close(data?: any) {
    this.modal.dismiss(data);
  }

  selection(option) {
    option.selected = !option.selected;
  }

  async save() {
    
    const paymentMethods = this.options.filter(x => x.selected);

    let gcash = paymentMethods.find(x => x.id == 2);
    if (gcash) {

      if (!this.frmGcash.valid) return this.showToastMessage("Enter 11 digit G-Cash Account", "danger");      
      else gcash.details = this.frmGcash.value;

    }

    let bankTransfer = paymentMethods.find(x => x.id == 3);
    if (bankTransfer) {

      if (!this.frmBankTransfer.valid) return this.showToastMessage("Please fill up bank details", "danger")
      else bankTransfer.details = this.frmBankTransfer.value;

    }


    console.log(paymentMethods)

    const loader = await this.loading.create({
      message: 'Saving...'
    });

    loader.present();     

    try {     

      setTimeout(() => {     
        

        this.sellerService.update({
          paymentMethods: paymentMethods
        });

        this.close(true);

        if (loader) loader.dismiss();
      }, 1000)
      
    } catch (error) {
      if (loader) loader.dismiss();
    }
  }

  async showToastMessage(message, color) {
    const toast = await this.toast.create({
      message: message,
      mode: 'ios',
      duration: 1000,
      color: color
    });

    toast.present();
  }

  async loader() {
    const loading = await this.loading.create({
      message: 'Saving...'
    });
    
    return loading;
  }
}
