import { Component, OnInit } from '@angular/core';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { DeliveryComponent } from './delivery/delivery.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { SellerService } from '../services/seller.service';

@Component({
  selector: 'app-seller',
  templateUrl: './seller.page.html',
  styleUrls: ['./seller.page.scss'],
})
export class SellerPage implements OnInit {
  title: string;
  process = 0;

  paymentMethods = [
    {
      id: 1,
      type: 'Cash on Delivery',
      selected: false
    },
    {
      id: 2,
      type: 'G-Cash',
      details: {},
      selected: false
    },
    {
      id: 3,
      type: 'Bank Transfer',
      details: {},
      selected: false
    }
  ];

  seller: any;
  storeName = '';

  constructor(private modal: ModalController, private actionSheet: ActionSheetController, private sellerService: SellerService) { 
    this.seller = this.sellerService.seller;
    if (this.seller) {
      this.title = this.seller.storeName;     
    }
    else this.next();
  }

  ngOnInit() {
  }

  back() {
    this.process--;
  }

  next() {
    this.process++;
  }

  finish() {
    this.seller = {};
    this.seller.storeName = this.storeName;

    localStorage.setItem('seller', JSON.stringify(this.seller))

    this.title = this.storeName;

    this.process = 0;
  }

  selection(option) {
    option.selected = !option.selected;
  }
}
