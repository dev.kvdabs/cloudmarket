import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController, ModalController } from '@ionic/angular';
import { SellerService } from 'src/app/services/seller.service';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss'],
})
export class DeliveryComponent implements OnInit {

  options = [
    {
        id: 1,
        type: 'Pickup',
        selected: false  
    },
    {
        id: 2,
        type: 'Deliver',
        selected: false    
    }
  ]

  frmPickup: FormGroup;

  pickup: boolean;

  constructor(private fb: FormBuilder, private modal: ModalController, private toast: ToastController, private sellerService: SellerService, private cdr: ChangeDetectorRef) { 
    this.frmPickup = this.fb.group({
        mobileNo: ['', Validators.required],
        address: ['', Validators.required],
        waze: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.sellerService.selectedDeliveryMethods.forEach(id => {
        let option = this.options.find(x => x.id == id)
        option.selected = true;
    });

    if (this.options.find(x => x.id == 1 && x.selected)) {

        const pickup = this.sellerService.seller.deliveryMethods.find(x => x.id == 1);
        this.initFrmPickup(pickup.details)
    }
  }

  initFrmPickup(data) {
    for (var key in data) {
        if (this.frmPickup.controls[key]) this.frmPickup.controls[key].setValue(data[key])
    }
  }

  close(data?: any) {
    this.modal.dismiss(data);
  }

  selection(option) {
    option.selected = !option.selected;
  }

  save() {
    if (this.options.find(x => x.id == 1 && x.selected) && !this.frmPickup.valid) {
        return this.showToastMessage("Fill up pickup details")
    }

    let deliveryMethods: any[] = [];

    this.options.filter(x => x.selected).forEach(x => {
        deliveryMethods.push({
            id: x.id,
            name: x.type,
            details: x.id == 1 ? this.frmPickup.value : null
        });
    })

    this.sellerService.update({
        deliveryMethods: deliveryMethods
    });

    this.close(true)
  }

  async showToastMessage(message) {
    const toast = await this.toast.create({
      message: message,
      mode: 'ios',
      duration: 1000,
      color: 'danger'
    });

    toast.present();
  }
}
