import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, NavParams } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ItemService } from 'src/app/services/item.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit {
  title = 'New Item';

  item: any = {};
  frmItem: FormGroup;

  constructor(private modal: ModalController, private fb: FormBuilder, private toast: ToastController, private itemService: ItemService, private params: NavParams) { 
    
  }

  ngOnInit() {
    this.frmItem = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      category: ['', Validators.required],
      unit: ['', Validators.required],
      price: ['', Validators.required],
      isAvailable: [true, Validators.required],
      isFeatured: [false, Validators.required]
    });

    let data = this.params.get("item") || {};
    
    for (var key in data) {
      if (this.frmItem.controls[key]) this.frmItem.controls[key].setValue(data[key])
    }

    if (data) this.title = data.name;
  }

  close(data?: any) {
    this.modal.dismiss(data);
  }

  save() {
    if (!this.frmItem.valid) {
      return this.showToast("Fill up item details");
    }

    let item = this.frmItem.value;
    if (!item.id) {
      item.id = new Date().getTime();

      this.itemService.add(item);
    } else {
      this.itemService.update(item)
    }

    this.close(item);
  }

  async showToast(message) {
    const toast = await this.toast.create({
      message: message,
      mode: 'ios',
      color: 'danger',
      duration: 1000
    });

    toast.present();
  }
}
