import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { ItemComponent } from '../seller-items/item/item.component';
import { ItemService } from 'src/app/services/item.service';

@Component({
  selector: 'app-seller-items',
  templateUrl: './seller-items.component.html',
  styleUrls: ['./seller-items.component.scss'],
})
export class SellerItemsComponent implements OnInit {
  view = 1;

  items = [];

  categories = [
    {
      name: 'Household'
    },
    {
      name: 'Alcoholic'
    },
    {
      name: 'Meat'
    }
  ]

  constructor(private modal: ModalController, private itemModal: ModalController, private alert: AlertController, private itemService: ItemService) { }

  ngOnInit() {

  }

  async ngAfterViewInit() {
    this.getItems();
  }

  async getItems() {
    this.items = await this.itemService.getItems();  
  }

  selectView(view) {
    this.view = view;
  }

  async openItem(item?) {
    const modal = await this.modal.create({
      component: ItemComponent,
      componentProps: {
        item: item 
      }
    });

    modal.onDidDismiss().then(res => {
      if (res && res.data) {
        this.getItems();
      }
    })

    modal.present();
  }

  close() {
    this.modal.dismiss();
  }

  async add() {
    if (this.view == 1) this.openItem();
    else this.category();
  }

  async item() {
    const modal = await this.itemModal.create({
      component: ItemComponent
    });

    modal.present();
  }

  async category(category?) {
    const alert = await this.alert.create({
      message: 'Category',
      inputs: [
        
      ]
    })
  }

  expand(category) {
    category.expand = !category.expand;

    if (category.expand) {
      category.items = this.items.filter(x => x.category == category.name);      

      if (category.items.length == 0) category.noItems = true;
    }
  }
}
