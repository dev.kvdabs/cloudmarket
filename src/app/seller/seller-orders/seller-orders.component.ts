import { Component, OnInit } from '@angular/core';
import { MaxLengthValidator } from '@angular/forms';

@Component({
  selector: 'app-seller-orders',
  templateUrl: './seller-orders.component.html',
  styleUrls: ['./seller-orders.component.scss'],
})
export class SellerOrdersComponent implements OnInit {

  statuses = [
    {
      id: 1,
      name: 'All',
      count: 9
    },
    {
      id: 2,
      name: 'Ordered',
      count: 5
    },
    {
      id: 3,
      name: 'Confirmed',
      count: 3
    },
    {
      id: 4,
      name: 'Completed',
      count: 1
    },
    {
      id: 5,
      name: 'Cancelled'
    }
  ];

  deliveryMethods = [
    {
      id: 1,
      name: 'Pickup'
    },
    {
      id: 2,
      name: 'Deliver'
    }
  ];

  paymentMethods = [
    {
      id: 1,
      name: 'Cash'    
    },
    {
      id: 2,
      name: 'G-Cash'    
    },
    {
      id: 3,
      name: 'Bank Transfer'    
    }
  ]

  selectedStatus = 1;

  orders = []

  _orders = [];

  constructor() { 
    let statusId, deliveryId, paymentId;


    statusId = this.getRandomInt(2, 6);
    deliveryId = this.getRandomInt(1, 3);
    paymentId = this.getRandomInt(1, 4);

    this.orders.push({
      id: 'Order: ' + new Date().getTime() + this.getRandomInt(150, 1000),  
      date: new Date().getTime(),
      name: 'Karl Dabuco',
      deliveryMethod: this.deliveryMethods.find(x => x.id == deliveryId).name,
      paymentMethod: this.paymentMethods.find(x => x.id == paymentId).name,
      statusId,
      status: this.statuses.find(x => x.id == statusId).name,
      statusColor: statusId == 3 ? "secondary" : statusId == 4 ? "success" : statusId == 5 ? "danger" : "",
      amount: this.getRandomInt(300, 5000)
    });

    statusId = this.getRandomInt(2, 6);
    deliveryId = this.getRandomInt(1, 3);
    paymentId = this.getRandomInt(1, 4);

    this.orders.push({
      id: 'Order: ' + new Date().getTime() + this.getRandomInt(150, 1000),  
      date: new Date().getTime(),
      name: 'Jeralda Creo',
      deliveryMethod: this.deliveryMethods.find(x => x.id == deliveryId).name,
      paymentMethod: this.paymentMethods.find(x => x.id == paymentId).name,
      statusId,
      status: this.statuses.find(x => x.id == statusId).name,
      statusColor: statusId == 3 ? "secondary" : statusId == 4 ? "success" : statusId == 5 ? "danger" : "",
      amount: this.getRandomInt(300, 5000)
    });

    statusId = this.getRandomInt(2, 6);
    deliveryId = this.getRandomInt(1, 3);
    paymentId = this.getRandomInt(1, 4);

    this.orders.push({
      id: 'Order: ' + new Date().getTime() + this.getRandomInt(150, 1000),  
      date: new Date().getTime(),
      name: 'Pingkaw',
      deliveryMethod: this.deliveryMethods.find(x => x.id == deliveryId).name,
      paymentMethod: this.paymentMethods.find(x => x.id == paymentId).name,
      statusId,
      status: this.statuses.find(x => x.id == statusId).name,
      statusColor: statusId == 3 ? "secondary" : statusId == 4 ? "success" : statusId == 5 ? "danger" : "",
      amount: this.getRandomInt(300, 5000)
    });

    statusId = this.getRandomInt(2, 6);
    deliveryId = this.getRandomInt(1, 3);
    paymentId = this.getRandomInt(1, 4);

    this.orders.push({
      id: 'Order: ' + new Date().getTime() + this.getRandomInt(150, 1000),  
      date: new Date().getTime(),
      name: 'John Doe',
      deliveryMethod: this.deliveryMethods.find(x => x.id == deliveryId).name,
      paymentMethod: this.paymentMethods.find(x => x.id == paymentId).name,
      statusId,
      status: this.statuses.find(x => x.id == statusId).name,
      statusColor: statusId == 3 ? "secondary" : statusId == 4 ? "success" : statusId == 5 ? "danger" : "",
      amount: this.getRandomInt(300, 5000)
    });

    statusId = this.getRandomInt(2, 6);
    deliveryId = this.getRandomInt(1, 3);
    paymentId = this.getRandomInt(1, 4);

    this.orders.push({
      id: 'Order: ' + new Date().getTime() + this.getRandomInt(150, 1000),  
      date: new Date().getTime(),
      name: 'Ate Girl',
      deliveryMethod: this.deliveryMethods.find(x => x.id == deliveryId).name,
      paymentMethod: this.paymentMethods.find(x => x.id == paymentId).name,
      statusId,
      status: this.statuses.find(x => x.id == statusId).name,
      statusColor: statusId == 3 ? "secondary" : statusId == 4 ? "success" : statusId == 5 ? "danger" : "",
      amount: this.getRandomInt(300, 5000)
    });

    statusId = this.getRandomInt(2, 6);
    deliveryId = this.getRandomInt(1, 3);
    paymentId = this.getRandomInt(1, 4);

    this.orders.push({
      id: 'Order: ' + new Date().getTime() + this.getRandomInt(150, 1000),  
      date: new Date().getTime(),
      name: 'Boy Isog',
      deliveryMethod: this.deliveryMethods.find(x => x.id == deliveryId).name,
      paymentMethod: this.paymentMethods.find(x => x.id == paymentId).name,
      statusId,
      status: this.statuses.find(x => x.id == statusId).name,
      statusColor: statusId == 3 ? "secondary" : statusId == 4 ? "success" : statusId == 5 ? "danger" : "",
      amount: this.getRandomInt(300, 5000)
    });

    statusId = this.getRandomInt(2, 6);
    deliveryId = this.getRandomInt(1, 3);
    paymentId = this.getRandomInt(1, 4);

    this.orders.push({
      id: 'Order: ' + new Date().getTime() + this.getRandomInt(150, 1000),  
      date: new Date().getTime(),
      name: 'Jeraldine Goltiano Creo Tambalolo',
      deliveryMethod: this.deliveryMethods.find(x => x.id == deliveryId).name,
      paymentMethod: this.paymentMethods.find(x => x.id == paymentId).name,
      statusId,
      status: this.statuses.find(x => x.id == statusId).name,
      statusColor: statusId == 3 ? "secondary" : statusId == 4 ? "success" : statusId == 5 ? "danger" : "",
      amount: this.getRandomInt(300, 5000)
    });

    this._orders = this.orders;

    this.statuses[0].count = this.orders.length;

    for (var i=1; i<this.statuses.length; i++) {      
      this.statuses[i].count = this.orders.filter(x => x.statusId == this.statuses[i].id).length;
    }
  }

  ngOnInit() {}

  selectStatus(statusId) {
    this.selectedStatus = statusId;

    if (this.selectedStatus == 1) this._orders = this.orders;
    else this._orders = this.orders.filter(x => x.statusId == this.selectedStatus);
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    const rand = Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive

    return rand
  }

}
