import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SellerPageRoutingModule } from './seller-routing.module';

import { SellerPage } from './seller.page';
import { DeliveryComponent } from './delivery/delivery.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';

import { ItemComponent } from './seller-items/item/item.component';
import { SellerHomeComponent } from './seller-home/seller-home.component';
import { SellerOrdersComponent } from './seller-orders/seller-orders.component';
import { SellerSetupComponent } from './seller-setup/seller-setup.component';
import { SellerItemsComponent } from './seller-items/seller-items.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    SellerPageRoutingModule
  ],
  declarations: [SellerPage, DeliveryComponent, PaymentMethodComponent, ItemComponent, SellerHomeComponent, SellerOrdersComponent, SellerSetupComponent, SellerItemsComponent]  
})
export class SellerPageModule {}
