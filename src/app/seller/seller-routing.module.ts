import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SellerPage } from './seller.page';
import { SellerHomeComponent } from './seller-home/seller-home.component';
import { SellerOrdersComponent } from './seller-orders/seller-orders.component';
import { SellerSetupComponent } from './seller-setup/seller-setup.component';
import { SellerItemsComponent } from './seller-items/seller-items.component';

const routes: Routes = [
  {
    path: '',
    component: SellerPage,

    children:[
      {
        path: '',
        redirectTo: 'home'
      },
      {
        path: 'home',
        component: SellerHomeComponent
      },
      {
        path: 'orders',
        component: SellerOrdersComponent
      },
      {
        path: 'items',
        component: SellerItemsComponent
      },
      {
        path: 'setup',
        component: SellerSetupComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellerPageRoutingModule {}
