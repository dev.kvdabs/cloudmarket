import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuyerPage } from './buyer.page';
import { BuyerOrdersComponent } from './buyer-orders/buyer-orders.component';

const routes: Routes = [
  {
    path: '',
    component: BuyerPage,
    children:[
      {
        path: '',
        redirectTo: 'stores'
      },
      {
        path: 'stores',        
        loadChildren: '../sellers/sellers.module#SellersPageModule'
      },
      {
        path: 'orders',
        component: BuyerOrdersComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuyerPageRoutingModule {}
