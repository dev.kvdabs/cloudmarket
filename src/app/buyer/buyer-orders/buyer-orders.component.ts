import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buyer-orders',
  templateUrl: './buyer-orders.component.html',
  styleUrls: ['./buyer-orders.component.scss'],
})
export class BuyerOrdersComponent implements OnInit {

  statuses = [
    {
      id: 2,
      name: 'Ordered',
      count: 5
    },
    {
      id: 4,
      name: 'Completed',
      count: 1
    },
    {
      id: 5,
      name: 'Cancelled'
    }
  ];

  selectedStatus = 2;
  orders = [];

  constructor() { }

  ngOnInit() {
    this.orders.push({
      id: 'Order: ' + new Date().getTime(),  
      date: new Date().getTime(),
      seller: 'ErlinDa\'best',
      buyer: 'Karl Dabuco',
      deliveryMethod: 'Pickup',
      paymentMethod: 'G-Cash',
      statusId: 2,
      status: 'Ordered',
      statusColor: "secondary",
      amount: 1845
    });
  }

  selectStatus(statusId) {
    this.selectedStatus = statusId;

    // if (this.selectedStatus == 1) this._orders = this.orders;
    // else this._orders = this.orders.filter(x => x.statusId == this.selectedStatus);
  }
}
