import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-buyer',
  templateUrl: './buyer.page.html',
  styleUrls: ['./buyer.page.scss'],
})
export class BuyerPage implements OnInit {

  constructor(private nav: NavController, private actionSheet: ActionSheetController) { }

  ngOnInit() {
  }

  async login() {
    this.nav.navigateRoot('/seller')
  }
}
