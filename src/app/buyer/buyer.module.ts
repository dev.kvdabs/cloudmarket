import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuyerPageRoutingModule } from './buyer-routing.module';

import { BuyerPage } from './buyer.page';
import { BuyerOrdersComponent } from './buyer-orders/buyer-orders.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuyerPageRoutingModule
  ],
  declarations: [BuyerPage, BuyerOrdersComponent]
})
export class BuyerPageModule {}
