import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-sellers',
  templateUrl: './sellers.page.html',
  styleUrls: ['./sellers.page.scss'],
})
export class SellersPage implements OnInit {
  sellers = [
    {
      name: 'AA Store',
      logoText: 'AS',
      category: 'Groceries, Veggies'
    },
    {
      name: 'Aeon\'s Dessert House',
      logoText: 'AH',
      category: 'Desserts, Snacks, Juice'
    },
    {
      name: 'Inday Veggies',
      logoText: 'IV',
      category: 'Veggies, Spices'
    },
    {
      name: 'ABC Clothing',
      logoText: 'AC',
      category: 'Clothes, Sportswear'
    },
    {
      name: 'AA Store',
      logoText: 'AS',
      category: 'Groceries, Veggies'
    },
    {
      name: 'Aeon\'s Dessert House',
      logoText: 'AH',
      category: 'Desserts, Snacks, Juice'
    },
    {
      name: 'Inday Veggies',
      logoText: 'IV',
      category: 'Veggies, Spices'
    },
    {
      name: 'ABC Clothing',
      logoText: 'AC',
      category: 'Clothes, Sportswear'
    }
  ]
  constructor(private nav: NavController, private actionSheet: ActionSheetController) { }

  ngOnInit() {
  }

  gotoStore() {
    this.nav.navigateForward('/store')
  }

  async login() {
    this.nav.navigateRoot('/seller')
  }
}
