import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  CART_KEY = '_cloudmarket-cart';
  items = [];
  total = 0;

  constructor(private nav: NavController) { 
    this.items = localStorage.getItem(this.CART_KEY) ? JSON.parse(localStorage.getItem(this.CART_KEY)) : [];
    this.computeCartTotal();
  }

  ngOnInit() {
  }

  back() {
    this.nav.back();
  }

  increment(item) {
    item.quantity++;    
    this.updateCart(item);
  }

  decrement(item) {
    item.quantity--;

    if (item.quantity <= 0) {
      this.items = this.items.filter(x => x.id !== item.id);
      this.saveToStorage();
    } else {
      this.updateCart(item);
    }    
  }

  remove(item) {
    this.items = this.items.filter(x => x.id !== item.id);
    this.computeCartTotal();
    this.saveToStorage();
  }

  updateCart(item) {
    item['price'] = item.price;
    item['quantity'] = item.quantity;
    item['total'] = item.price * item.quantity;

    this.computeCartTotal();
    this.saveToStorage();
  }

  computeCartTotal() {
    this.total = this.items.reduce( (tot, record) => {
      return tot + record['total'];
    }, 0);
  }

  saveToStorage() {
    localStorage.setItem(this.CART_KEY, JSON.stringify(this.items));
  }

  checkout() {
    this.nav.navigateForward("/checkout");
  }
}
